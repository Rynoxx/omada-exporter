import os
import sys
import logging

from prometheus_client import start_http_server
from app.core.omada_metric_retriever import OmadaAPMetricRetriever


LOG_LEVEL = os.getenv("LOG_LEVEL", "INFO")
logging.basicConfig(
        level=LOG_LEVEL,
        )

log = logging.getLogger('main')

HTTP_BIND_PORT = int(os.getenv("HTTP_BIND_PORT", 8000))

OMADA_USER = os.getenv("OMADA_USER", "")
OMADA_PASSWORD = os.getenv("OMADA_PASSWORD", "")

OMADA_HOST = os.getenv("OMADA_HOST", "")
OMADA_PORT = os.getenv("OMADA_PORT", "8043")
OMADA_URL =  "https://{}:{}/".format(OMADA_HOST, OMADA_PORT)

OMADA_AP_METRIC_SCRAPING_INTERVAL = int(os.getenv("OMADA_AP_METRIC_SCRAPING_INTERVAL", "60"))


def main():

    omada_metric_retriever = OmadaAPMetricRetriever("OmadaAPMetrics", OMADA_AP_METRIC_SCRAPING_INTERVAL, OMADA_URL, OMADA_USER, OMADA_PASSWORD)
    omada_metric_retriever.start()

    log.info("Start prometheus (exposer) http server on port %d", HTTP_BIND_PORT)
    start_http_server(HTTP_BIND_PORT)

    while True:
        omada_metric_retriever.join(timeout=2.0)
        if not omada_metric_retriever.is_alive():
            log.error("An error occured on omada metric thread, exiting...")
            sys.exit(1)

if __name__ == '__main__':
    main()
